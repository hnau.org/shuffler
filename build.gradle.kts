plugins {
    val kotlinVersion: String by System.getProperties()
    kotlin("jvm") version kotlinVersion apply false
    val kspVersion: String by System.getProperties()
    id("com.google.devtools.ksp") version kspVersion apply false
}

buildscript {
    dependencies {
        classpath(kotlin("gradle-plugin"))
    }
}

allprojects {
    repositories {
        mavenCentral()
    }
    group = "hnau.shuffler"
    version = "1.0.8"

    tasks.withType(org.jetbrains.kotlin.gradle.tasks.KotlinCompile::class).all {
        kotlinOptions.freeCompilerArgs = listOf("-Xcontext-receivers")
    }
}