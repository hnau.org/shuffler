package hnau.shuffler.test.first.data

@JvmInline
value class Login(
    val login: String,
)