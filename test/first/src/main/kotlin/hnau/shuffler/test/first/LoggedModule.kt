package hnau.shuffler.test.first

import hnau.shuffler.annotations.Shuffle
import hnau.shuffler.test.first.data.AccessToken
import hnau.shuffler.test.first.data.Login

class LoggedModule(
    dependencies: Dependencies,
) {

    @Shuffle
    interface Dependencies {

        val login: Login

        fun connected(
            accessToken: AccessToken,
        ): ConnectedModule.Dependencies
    }

    val connected = ConnectedModule(
        dependencies = dependencies.connected(
            accessToken = AccessToken("access_token")
        ),
    )

    init {
        println("Login: ${dependencies.login.login}")
    }
}