package hnau.shuffler.test.first.data

@JvmInline
value class AccessToken(
    val accessToken: String,
)