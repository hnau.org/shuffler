package hnau.shuffler.test.first

import hnau.shuffler.annotations.Shuffle
import hnau.shuffler.test.first.data.AccessToken

class ConnectedModule(
    dependencies: Dependencies,
) {

    @Shuffle
    interface Dependencies {

        val accessToken: AccessToken

        fun ready(): ReadyModel.Dependencies
    }

    val ready = ReadyModel(
        dependencies = dependencies.ready(),
    )

    init {
        println("Access token: ${dependencies.accessToken.accessToken}")
    }

}