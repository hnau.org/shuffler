package hnau.shuffler.test.first.data

fun interface DoLogout {

    fun logout()
}