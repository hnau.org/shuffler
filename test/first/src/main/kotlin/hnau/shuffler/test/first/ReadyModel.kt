package hnau.shuffler.test.first

import hnau.shuffler.annotations.Shuffle
import hnau.shuffler.test.first.data.DoLogout

class ReadyModel(
    dependencies: Dependencies,
) {

    @Shuffle
    interface Dependencies {

        val list: List<Int>

        val doLogout: DoLogout

        fun logged(): LoggedModule.Dependencies

        companion object
    }

    init {
        dependencies.doLogout.logout()
    }
}