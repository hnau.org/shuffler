package hnau.shuffler.test.second.data

import hnau.shuffler.test.first.data.Login

fun interface DoLogin {

    fun login(login: Login)
}