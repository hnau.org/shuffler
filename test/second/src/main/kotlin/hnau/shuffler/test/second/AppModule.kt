package hnau.shuffler.test.second

class AppModule {

    init {
        val login = LoginModule(
            dependencies = LoginModule.Dependencies.impl(
                doLogin = {

                },
                doLogout = {

                },
                list = listOf(1),
            )
        )
    }
}