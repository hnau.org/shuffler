package hnau.shuffler.test.second

import hnau.shuffler.annotations.Shuffle
import hnau.shuffler.test.first.LoggedModule
import hnau.shuffler.test.first.data.Login
import hnau.shuffler.test.second.data.DoLogin

class LoginModule(
    dependencies: Dependencies,
) {

    @Shuffle
    interface Dependencies {

        val doLogin: DoLogin

        fun logged(
            login: Login,
        ): LoggedModule.Dependencies

        companion object
    }

    init {
        val login = Login("Login")
        dependencies.doLogin.login(login)
        LoggedModule(
            dependencies = dependencies.logged(login),
        )
    }
}