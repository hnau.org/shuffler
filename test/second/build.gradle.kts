plugins {
    id("com.google.devtools.ksp")
    kotlin("jvm")
}

dependencies {
    implementation(libs.kotlin.stdlib.jdk8)
    implementation(project(mapOf("path" to ":shuffler:annotations")))
    ksp(project(":shuffler:processor"))
    implementation(project(mapOf("path" to ":test:first")))
}