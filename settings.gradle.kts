rootProject.name = "Shuffler"

include(":shuffler:annotations")
include(":shuffler:processor")
include(":test:first")
include(":test:second")

pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}