plugins {
    kotlin("jvm")
    id("maven-publish")
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(kotlin.sourceSets.main.get().kotlin)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            artifactId = project.name
            version = project.version.toString()
            from(components["kotlin"])
            artifact(tasks["sourcesJar"])
        }
    }
}

dependencies {
    implementation(libs.kotlin.stdlib.jdk8)
    implementation(libs.ksp.api)
    implementation(libs.kotlinpoet.main)
    implementation(libs.kotlinpoet.ksp)
    implementation(project(mapOf("path" to ":shuffler:annotations")))
}