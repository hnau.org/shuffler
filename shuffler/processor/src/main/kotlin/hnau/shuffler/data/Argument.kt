package hnau.shuffler.data

import com.google.devtools.ksp.symbol.KSType

internal data class Argument(
    val name: String,
    val type: KSType,
)