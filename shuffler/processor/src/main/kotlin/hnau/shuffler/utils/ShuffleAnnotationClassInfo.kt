package hnau.shuffler.utils

import hnau.shuffler.annotations.Shuffle
import kotlin.reflect.KClass

internal object ShuffleAnnotationClassInfo {

    private val annotationClass: KClass<Shuffle> = Shuffle::class

    val nameWithPackage: String
        get() = annotationClass.qualifiedName!!

    val simpleName: String
        get() = annotationClass.simpleName!!
}