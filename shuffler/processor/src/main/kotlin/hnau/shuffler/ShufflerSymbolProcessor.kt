package hnau.shuffler

import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.processing.SymbolProcessor
import com.google.devtools.ksp.symbol.KSAnnotated
import com.google.devtools.ksp.validate
import hnau.shuffler.impl.InterfaceToImplement
import hnau.shuffler.impl.create
import hnau.shuffler.impl.implement
import hnau.shuffler.utils.ShuffleAnnotationClassInfo

class ShufflerSymbolProcessor(
    private val codeGenerator: CodeGenerator,
    private val logger: KSPLogger,
) : SymbolProcessor {

    override fun process(
        resolver: Resolver,
    ): List<KSAnnotated> {

        val annotateds = resolver
            .getSymbolsWithAnnotation(ShuffleAnnotationClassInfo.nameWithPackage)
            .groupBy { it.validate() }

        val interfacesToImplement = annotateds[true]
            ?.map { annotated ->
                InterfaceToImplement.create(
                    annotated = annotated,
                )
            }
            ?: emptyList()

        with(resolver) {
            with(logger) {
                interfacesToImplement.implement(
                    codeGenerator = codeGenerator,
                )
            }
        }

        return annotateds[false] ?: emptyList()
    }
}