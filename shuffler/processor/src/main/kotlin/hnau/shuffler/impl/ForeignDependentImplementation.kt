package hnau.shuffler.impl

import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSName
import hnau.shuffler.data.Argument
import hnau.shuffler.ext.argumants
import hnau.shuffler.ext.implementationName
import hnau.shuffler.ext.log

internal data class ForeignDependentImplementation(
    override val impl: KSName,
    override val arguments: List<Argument>,
) : Dependent {

    companion object {

        context(Resolver)
        fun create(
            interfaceDeclaration: KSClassDeclaration,
        ): ForeignDependentImplementation {
            val implementationName = interfaceDeclaration.implementationName
            val implementation = getClassDeclarationByName(implementationName)
                ?: error("Unable find implementation (${implementationName.log}) of ${interfaceDeclaration.log}")
            val constructor = implementation
                .primaryConstructor
                ?: error("${implementation.log} has no primary constructor")
            return ForeignDependentImplementation(
                impl = implementationName,
                arguments = constructor.argumants,
            )
        }
    }
}