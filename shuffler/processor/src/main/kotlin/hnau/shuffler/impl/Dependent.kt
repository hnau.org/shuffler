package hnau.shuffler.impl

import com.google.devtools.ksp.symbol.KSName
import hnau.shuffler.data.Argument

internal sealed interface Dependent {

    val impl: KSName

    val arguments: List<Argument>
}